// This file was auto-generated

public enum AiInputType : ushort { MyHealth, MyAttackCooldown, MyAttackRange, MyHealAmount, MyDistanceToTarget, MyDistanceToHealth }
public static class AiInputTypeSize { public const ushort Size = 6; }
public enum AiConsiderationType : ushort { WhenMyHeathIsGood, WhenMyHealthIsBad, WhenMyAttackIsReady, WhenMyAttackIsNotReady, WhenMyHealthIsIncreasing, WhenMyTargetIsOutOfAttackRange, WhenMyTargetIsInAttackRange, WhenMyTargetIsTooFar, WhenHealthIsNear, WhenMyTargetIsNotTooFar }
public static class AiConsiderationTypeSize { public const ushort Size = 10; }
public enum AiDecisionType : ushort { MoveToHealthStation, IdleHeal, IdleNoTarget, IdleBetweenAttacks, Attack, MoveToTarget }
public static class AiDecisionTypeSize { public const ushort Size = 6; }
public enum AiProfileType : ushort { SimpleAgent }
public static class AiProfileTypeSize { public const ushort Size = 1; }
