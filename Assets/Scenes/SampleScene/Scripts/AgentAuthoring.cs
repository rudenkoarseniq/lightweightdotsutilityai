// This example authoring component just adds all the components necessary for a simple example agent.
// Typically there might be separate authoring tools for different factions (player, enemy, etc),
// But this is just an example so I'm re-using it for both agent types.

using Unity.Entities;
using UnityEngine;

public class AgentAuthoring : MonoBehaviour
{
    [Tooltip("The agent's health capacity.")]
    public float DefaultHealth = 10f;

    [Tooltip("The interval at which the agent attacks.")]
    public float AttackInterval = 1f;

    [Tooltip("The agent's attack range.")]
    public float AttackRange = 2f;

    [Tooltip("The amount of damage an agent does for a single attack.")]
    public float AttackDamage = 1f;

    [Tooltip("The agent's movment speed speed. Player entities ignore this.")]
    public float MoveSpeed = 4f;

    public bool IsPlayer = false;
}

public class AgentBaker : Baker<AgentAuthoring>
{
    public override void Bake(AgentAuthoring authoring)
    {
        Entity entity = GetEntity(authoring, TransformUsageFlags.None);

        AddComponent<AgentTag>(entity);
        AddComponent(entity, new HealthData { Health = authoring.DefaultHealth });
        AddComponent(entity, new MaxHealthData { Health = authoring.DefaultHealth });
        AddComponent(entity, new AttackTargetData { TargetEntity = Entity.Null, Distance = float.MaxValue });
        AddComponent(entity, new AttackCooldownData { Time = authoring.AttackInterval, Interval = authoring.AttackInterval });
        AddComponent(entity, new AttackData { Range = authoring.AttackRange, Damage = authoring.AttackDamage });
        AddComponent(entity, new TargetData { TargetEntity = Entity.Null });
        AddComponent(entity, new HealthInfluenceData { NearestHealthStationEntity = Entity.Null, Amount = 0f, DistanceToNearestStation = float.MaxValue });
        AddComponent(entity, new MoveSpeedData { Speed = authoring.MoveSpeed });

        if (authoring.IsPlayer) {
            AddComponent<PlayerTag>(entity);
            AddComponent(entity, new FactionData { Faction = FactionType.Player });
        } else {
            AddComponent<EnemyTag>(entity);
            AddComponent(entity, new FactionData { Faction = FactionType.Enemy });
        }
    }
}