// This scriptable object defines a list of considerations.
// **Note that it requires BoundedCurve, which can be found in CurvePropertyDrawer.cs

// **Note - it is very inefficient right now - anytime you modify a setting, it will redraw all the curves.
//   Unfortunately, there is no easy way around this, so for now it will have to suffice.
//   This inefficiency only affects the editor.

using UnityEngine;
using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace Lwduai
{

    [System.Serializable]
    public class AiRangeEntry
    {
        [Tooltip("Checking this will allow the min value to be obtained from an input. (default false)")]
        public bool MinIsVariable = false;

        [Tooltip("Checking this will allow the max value to be obtained from an input. (default false)")]
        public bool MaxIsVariable = false;

        [Tooltip("The static value used if the minIsVariable is unchecked. (default 0)")]
        [HideIf("MinIsVariable")]
        public float Min = 0f;

        [Tooltip("The static value used if the maxIsVariable is unchecked. (default 1)")]
        [HideIf("MaxIsVariable")]
        public float Max = 1f;

        [Tooltip("The input value to use if minIsVariable is checked. (default first input)")]
        [ShowIf("MinIsVariable")]
        [ValueDropdown("@AiOdinUtils.RunInputNameQuery()")]
        public string MinInputName;

        [Tooltip("The input value to use if maxIsVariable is checked. (default first input)")]
        [ShowIf("MaxIsVariable")]
        [ValueDropdown("@AiOdinUtils.RunInputNameQuery()")]
        public string MaxInputName;

        // returns the id of the min value, converted from a string
        // it must be set to dynamic and have a MinInputName!
        public AiInputType MakeMinId() { return (AiInputType)System.Enum.Parse(typeof(AiInputType), MinInputName); }

        // returns the id of the max value, converted from a string
        // it must be set to dynamic and have a MaxInputName!
        public AiInputType MakeMaxId() { return (AiInputType)System.Enum.Parse(typeof(AiInputType), MaxInputName); }
    }


    [System.Serializable]
    public class AiConsiderationEntry
    {
        //[OnValueChanged("@AiUtils.RegenerateEnums()")]
        [Tooltip("Name of the consideration.")]
        public string Name = "New Consideration";

        [Tooltip("Specify the curve type. (default Linear)")]
        public CurveData Curve = new CurveData { M = 0, K = 0, B = 0, C = 0, CurveType = CurveType.Linear };

        [ValueDropdown("@AiOdinUtils.RunInputNameQuery()", ExcludeExistingValuesInList = true)]
        [Tooltip("The input to use for the value in this consideration. (default first input)")]
        public string InputName;

        [Tooltip("Default range is 0 to 1. Enable this to set custom ranges. (default disabled)")]
        public bool EnableRanges = false;

        [ShowIf("EnableRanges")]
        public AiRangeEntry Ranges;

        [BoundedCurve(0, 0, 1, 1, 2)]
        [SerializeField] 
        private AnimationCurve CurveDisplay = new AnimationCurve();

        // returns the id of the consideration, converted from a string
        public AiConsiderationType MakeId() { return (AiConsiderationType)System.Enum.Parse(typeof(AiConsiderationType), Name); }

        // returns the id of the input value, converted from a string
        public AiInputType MakeInputId() { return (AiInputType)System.Enum.Parse(typeof(AiInputType), InputName); }

        //// Nonlinear version
        //public void RefreshCurveDisplay()
        //{
        //    CurveDisplay.keys = new Keyframe[0];
        //    for (int i = 0; i <= 20; i++) {
        //        float sample = i * 0.05f;
        //        CurveDisplay.AddKey(sample, CurveUtils.Evaluate(sample, Curve));
        //    }
        //}

        // Linear version
        public void RefreshCurveDisplay()
        {
            CurveDisplay.keys = new Keyframe[0];
            for (int s = 0; s <= 20; s++) {
                float sample = s * 0.05f;
                CurveDisplay.AddKey(sample, CurveUtils.Evaluate(sample, Curve));
            }

            for (int i = 0; i < CurveDisplay.length; i++) {
                UnityEditor.AnimationUtility.SetKeyLeftTangentMode(CurveDisplay, i, UnityEditor.AnimationUtility.TangentMode.Linear);
                UnityEditor.AnimationUtility.SetKeyRightTangentMode(CurveDisplay, i, UnityEditor.AnimationUtility.TangentMode.Linear);
            }
        }
    }


    [CreateAssetMenu(fileName = "New AI Consideration Table", menuName = "Game/AI Consideration Table")]
    public class AiConsiderationTableRecord : ScriptableObject
    {
        //[OnValueChanged("@AiUtils.RegenerateEnums()")]
        public List<AiConsiderationEntry> Considerations;

        #if UNITY_EDITOR
            public void OnValidate()
            {
                for (int i = 0; i < Considerations.Count; i++) {
                    Considerations[i].RefreshCurveDisplay();
                }
            }
        #endif
    }
}