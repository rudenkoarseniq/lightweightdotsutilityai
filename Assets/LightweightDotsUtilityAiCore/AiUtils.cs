// holds various data structures and functions for Utility AI
// (mainly building the blob asset and scoring)
//-----------------------------------------------------------//

using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace Lwduai
{

    // ##################### AITABLE STATIC CLASS ################## //

    public static class AiUtils
    {
        // takes an AiTableEntry (which wraps up scriptable-objects into a single object), and returns a blob asset reference.
        public static BlobAssetReference<AiTableDef> CreateBlobAsset(AiTableEntry entry)
        {
            using BlobBuilder builder = new BlobBuilder(Allocator.Temp); // using ensures that it's auto-disposed
            ref AiTableDef blobAsset = ref builder.ConstructRoot<AiTableDef>();

            // input array
            BlobBuilderArray<AiInputDef> inputs = builder.Allocate(ref blobAsset.Inputs, entry.Inputs.Count);
            for (ushort i = 0; i < entry.Inputs.Count; i++) {
                AiInputEntry input = entry.Inputs[i];
                // set the value of this input.
                // ContributingConsiderationIds will be set below.
                inputs[i] = new AiInputDef { InputId = (AiInputType)i };

                // make a list of all consideration ids that use this input (either as a direct input or for min/max range value)
                List<AiConsiderationType> considerationIdList = new List<AiConsiderationType>();
                for (ushort c = 0; c < entry.Considerations.Count; c++) { // check ALL possible considerations
                    AiConsiderationEntry consideration = entry.Considerations[c];
                    if (consideration.InputName == input.Name) {
                        considerationIdList.Add(consideration.MakeId());
                    }
                    if (consideration.Ranges.MaxIsVariable && (consideration.Ranges.MaxInputName == input.Name)) {
                        considerationIdList.Add(consideration.MakeId());
                    }
                    if (consideration.Ranges.MinIsVariable && (consideration.Ranges.MinInputName == input.Name)) {
                        considerationIdList.Add(consideration.MakeId());
                    }
                }
                // allocate the considerationIds blob-array and fill it to match the previously created consideration id list.
                BlobBuilderArray<AiConsiderationType> considerationTypeRefs = builder.Allocate(ref inputs[i].ContributingConsiderationIds, considerationIdList.Count);
                for (ushort c = 0; c < considerationIdList.Count; c++) {
                    considerationTypeRefs[c] = considerationIdList[c];
                }
            }

            // consideration array
            BlobBuilderArray<AiConsiderationDef> considerations = builder.Allocate(ref blobAsset.Considerations, entry.Considerations.Count);
            for (ushort c = 0; c < entry.Considerations.Count; c++) {
                AiConsiderationEntry considerationDef = entry.Considerations[c];
                //RangeMaskType rangeMask = RangeMaskType.None;
                byte rangeMask = 0;
                float min = 0f, max = 1f; // default zero to one even if ranges are disabled
                if (considerationDef.EnableRanges) {
                    if (considerationDef.Ranges.MinIsVariable) {
                        rangeMask = BitmaskUtils.SetFlag(rangeMask, (byte)RangeMaskType.Min);
                        // interpret the min float as an integer corresponding to the input-id enum
                        min = (float)considerationDef.Ranges.MakeMinId();
                    } else {
                        min = considerationDef.Ranges.Min;
                    }
                    if (considerationDef.Ranges.MaxIsVariable) {
                        rangeMask = BitmaskUtils.SetFlag(rangeMask, (byte)RangeMaskType.Max);
                        // interpret the max float as an integer corresponding to the input-id enum
                        max = (float)considerationDef.Ranges.MakeMaxId();
                    } else {
                        max = considerationDef.Ranges.Max;
                    }
                }
                // set the value of this consideration.
                // ContributingDecisionIds will be set below.
                considerations[c] = new AiConsiderationDef {
                    ConsiderationId = (AiConsiderationType)c,
                    Curve = considerationDef.Curve,
                    InputId = considerationDef.MakeInputId(),
                    RangeMask = rangeMask,
                    Min = min,
                    Max = max
                 };

                // make a list of all decision ids that use this input
                List<AiDecisionType> decisionIdList = new List<AiDecisionType>();
                for (ushort d = 0; d < entry.Decisions.Count; d++) {
                    AiDecisionEntry decision = entry.Decisions[d];
                    foreach (string considerationName in decision.ConsiderationNames) {
                        if (considerationName == considerationDef.Name) {
                            decisionIdList.Add(decision.MakeId());
                        }
                    }
                }
                // allocate the decisionIds blob-array and fill it to match the previously created decision id list.
                BlobBuilderArray<AiDecisionType> refs = builder.Allocate(ref considerations[c].ContributingDecisionIds, decisionIdList.Count);
                for (ushort d = 0; d < decisionIdList.Count; d++) {
                    refs[d] = decisionIdList[d];
                }
            }

            // decision array
            BlobBuilderArray<AiDecisionDef> destDecisions = builder.Allocate(ref blobAsset.Decisions, entry.Decisions.Count);
            for (ushort d = 0; d < entry.Decisions.Count; d++) {
                ref AiDecisionDef destDecision = ref destDecisions[d];
                AiDecisionEntry srcDecision = entry.Decisions[d];
                BlobBuilderArray<AiConsiderationType> considerationIds = builder.Allocate(ref destDecision.ConsiderationIds, srcDecision.ConsiderationNames.Count);
                for (ushort c = 0; c < srcDecision.ConsiderationNames.Count; c++) {
                    considerationIds[c] = srcDecision.MakeConsiderationId(c);
                }
                destDecisions[d].DecisionId = (AiDecisionType)d;
                destDecisions[d].IsEnabled = srcDecision.IsEnabled;
                destDecisions[d].Weight = srcDecision.Weight;
            }

            // profile array
            BlobBuilderArray<AiProfileDef> profiles = builder.Allocate(ref blobAsset.Profiles, entry.Profiles.Count);
            for (ushort p = 0; p < entry.Profiles.Count; p++) {
                ref AiProfileDef destProfile = ref profiles[p];
                AiProfileEntry srcProfile = entry.Profiles[p];
                profiles[p].ProfileId = (AiProfileType)p;
                BlobBuilderArray<AiDecisionType> destDecisionIds = builder.Allocate(ref destProfile.DecisionIds, srcProfile.DecisionNames.Count);
                for (ushort d = 0; d < srcProfile.DecisionNames.Count; d++) {
                    destDecisionIds[d] = srcProfile.MakeDecisionId(d);
                }
            }

            // create blob asset reference
            BlobAssetReference<AiTableDef> blobRef = builder.CreateBlobAssetReference<AiTableDef>(Allocator.Persistent);

            //builder.Dispose();
            return blobRef;
        }

        // return a string containing debugging info for the specified entity.
        public static string MakeAiDetailString(EntityManager em, BlobAssetReference<AiTableDef> table, Entity entity)
        {
            DynamicBuffer<AiDecisionScoreData> decisionScores = em.GetBuffer<AiDecisionScoreData>(entity);
            DynamicBuffer<AiConsiderationScoreData> considerationScores = em.GetBuffer<AiConsiderationScoreData>(entity);
            DynamicBuffer<AiInputData> inputs = em.GetBuffer<AiInputData>(entity);
            AiActionData winner = em.GetComponentData<AiActionData>(entity);

            if ((ushort)winner.Score != ushort.MaxValue) {
                string text = "";
                // each entity has one score value per decision
                for (ushort d = 0; d < decisionScores.Length; d++) {
                    AiDecisionScoreData decisionScore = decisionScores[d];
                    if (decisionScore.IsUsedByProfile) { // ignore actions not used by this profile
                        text += $"  {(AiDecisionType)d} --> {decisionScore.Score}\n";
                        // In order to get the scores for each agent's consideration, we need to find out which considerations this decision has.
                        // First, fetch the definition for the current decision.
                        ref AiDecisionDef decisionDefinition = ref table.Value.Decisions[d];
                        // for every consideration that this decision has...
                        for (ushort c = 0; c < decisionDefinition.ConsiderationIds.Length; c++) {
                            AiConsiderationType considerationId = decisionDefinition.ConsiderationIds[c];
                            AiInputType inputId = table.Value.Considerations[(ushort)considerationId].InputId;
                            text += $"    {considerationId} --> {considerationScores[(ushort)considerationId].Score} ({inputId} : {inputs[(ushort)inputId].Value})\n";
                        }
                    }
                }
                return $"[{entity.Index} : {winner.Action}, {winner.Score}]\n{text}";
            }
            return $"[{entity.Index} : No Winning Action]";
        }

        // Returns a simple string containing health data on the agent
        // H (health) is sorta game dependent.
        public static string MakeAiQuickInfoString(EntityManager em, BlobAssetReference<AiTableDef> table, Entity entity, short healthInputIndex)
        {
            AiActionData winner = em.GetComponentData<AiActionData>(entity);
            DynamicBuffer<AiInputData> inputs = em.GetBuffer<AiInputData>(entity);
            float health = inputs[healthInputIndex].Value;
            return $"{entity.Index} : {winner.Action}, S: {winner.Score} H:{health}";
        }

        // returns val scaled between 0 and 1
        // if val is smaller than oldMin it will be clamped to oldMin
        // if val is larger than oldMax it will be clamped to oldMax
        public static float ScaleToClampedUnitInterval(float val, float oldMin, float oldMax)
        {
            return (math.clamp(val, oldMin, oldMax) - oldMin) / (oldMax - oldMin);
        }

        // This function attempts to get a table reference from the default world.
        // If it fails, a default reference will be returned, which can be checked against IsCreated to determine success or failure.
        public static BlobAssetReference<AiTableDef> TryGetDwAiTableReference()
        {
            if (!World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(ComponentType.ReadOnly<AiTableData>()).TryGetSingleton(out AiTableData table)) { return default; }
            return table.Reference;
        }

        // This function fetches a definition reference from the default world.
        public static BlobAssetReference<AiTableDef> GetDwAiTableReference()
        {
            return World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(ComponentType.ReadOnly<AiTableData>()).GetSingleton<AiTableData>().Reference;
        }

        // Teturn a hashset containing all of the decisions.
        // Note - I'm not using this right now...
        // I think it's faster to just loop over each decision and skip the ones not used by the specified profiles.
        // The allocation is probably slower.
        public static NativeHashSet<ushort> MakeDecisionHashSet(in DynamicBuffer<AiProfileData> profiles, Allocator allocator, in AiAgentAspect ai)
        {
            // first get a set of all decisions used by the profile.
            NativeHashSet<ushort> mergedDecisionIds = new NativeHashSet<ushort>(0, allocator);
            for (ushort p = 0; p < profiles.Length; p++) {
                ref AiProfileDef profileDef = ref ai.DefTable.Profiles[(ushort)profiles[p].ProfileId];
                // for every decision in the profile...
                for (ushort d = 0; d < profileDef.DecisionIds.Length; d++) {
                    mergedDecisionIds.Add((ushort)profileDef.DecisionIds[d]);
                }
            }
            return mergedDecisionIds;
        }

        // This function will add/remove frame number tags to the specified entity based on the specified frame.
        // It will remove the previous tag and add a new tag.
        public static void CycleFrameComponent(sbyte frameNumber, sbyte lastFrame, ref SystemState state, Entity entity)
        {
            if (frameNumber == lastFrame) { state.EntityManager.AddComponent<AiLastFrameTag>(entity); }

            // at 30fps with 7 frames total (LF is last frame)
            //  1   2   3  4  5  6  7      1   2   3  4  5  6  7      1   2   1
            // -1, -1, -1, 0, 1, 2, 3 LF, -1, -1, -1, 0, 1, 2, 3 LF, -1, -1, -1...

            switch (frameNumber) {
                case -1:
                    if (lastFrame == 9) { state.EntityManager.RemoveComponent<AiFrame9Tag>(entity); }
                    state.EntityManager.RemoveComponent<AiLastFrameTag>(entity);
                    EnableFrameComponent(lastFrame, ref state, entity, false);
                    break;
                case 0:
                    state.EntityManager.AddComponent<AiFirstFrameTag>(entity);
                    break;
                case 1:
                    state.EntityManager.RemoveComponent<AiFirstFrameTag>(entity);
                    state.EntityManager.AddComponent<AiFrame1Tag>(entity);
                    break;
                case 2:
                    state.EntityManager.RemoveComponent<AiFrame1Tag>(entity);
                    state.EntityManager.AddComponent<AiFrame2Tag>(entity);
                    break;
                case 3:
                    state.EntityManager.RemoveComponent<AiFrame2Tag>(entity);
                    state.EntityManager.AddComponent<AiFrame3Tag>(entity);
                    break;
                case 4:
                    state.EntityManager.RemoveComponent<AiFrame3Tag>(entity);
                    state.EntityManager.AddComponent<AiFrame4Tag>(entity);
                    break;
                case 5:
                    state.EntityManager.RemoveComponent<AiFrame4Tag>(entity);
                    state.EntityManager.AddComponent<AiFrame5Tag>(entity);
                    break;
                case 6:
                    state.EntityManager.RemoveComponent<AiFrame5Tag>(entity);
                    state.EntityManager.AddComponent<AiFrame6Tag>(entity);
                    break;
                case 7:
                    state.EntityManager.RemoveComponent<AiFrame6Tag>(entity);
                    state.EntityManager.AddComponent<AiFrame7Tag>(entity);
                    break;
                case 8:
                    state.EntityManager.RemoveComponent<AiFrame7Tag>(entity);
                    state.EntityManager.AddComponent<AiFrame8Tag>(entity);
                    break;
                case 9:
                    state.EntityManager.RemoveComponent<AiFrame8Tag>(entity);
                    state.EntityManager.AddComponent<AiFrame9Tag>(entity);
                    break;
            }
        }

        // adds or removes a frame tag based on the specified frame number and toggle mode
        public static bool EnableFrameComponent(sbyte frameNumber, ref SystemState state, Entity entity, bool enable)
        {
            switch (frameNumber) {
                case 0: return enable ? state.EntityManager.AddComponent<AiFirstFrameTag>(entity) : state.EntityManager.RemoveComponent<AiFirstFrameTag>(entity);
                case 1: return enable ? state.EntityManager.AddComponent<AiFrame1Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame1Tag>(entity);
                case 2: return enable ? state.EntityManager.AddComponent<AiFrame2Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame2Tag>(entity);
                case 3: return enable ? state.EntityManager.AddComponent<AiFrame3Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame3Tag>(entity);
                case 4: return enable ? state.EntityManager.AddComponent<AiFrame4Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame4Tag>(entity);
                case 5: return enable ? state.EntityManager.AddComponent<AiFrame5Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame5Tag>(entity);
                case 6: return enable ? state.EntityManager.AddComponent<AiFrame6Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame6Tag>(entity);
                case 7: return enable ? state.EntityManager.AddComponent<AiFrame7Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame7Tag>(entity);
                case 8: return enable ? state.EntityManager.AddComponent<AiFrame8Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame8Tag>(entity);
                case 9: return enable ? state.EntityManager.AddComponent<AiFrame9Tag>(entity) : state.EntityManager.RemoveComponent<AiFrame9Tag>(entity);
            }
            return false;
        }
    }
} // namespace