// This singleton manager class serves two purposes:
//    1) It has a button to regenerate enums.
//    2) Provide a way to fetch lists from scriptable objects for use in the inspector.
//       (Because it's not always possible to access AiTableAuthoring which is in a subscene)
// This singleton monobehaviour should reside on a game-object in the scene.


using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace Lwduai
{
    public class AiManager : Singleton<AiManager>
    {
        [UnityEngine.HideInInspector]
        public AiInputTableRecord InputTable;

        [UnityEngine.HideInInspector]
        public AiConsiderationTableRecord ConsiderationTable;

        [UnityEngine.HideInInspector]
        public AiDecisionTableRecord DecisionTable;

        [UnityEngine.HideInInspector]
        public AiProfileTableRecord ProfileTable;

        public string m_enumOutputFolder = "Assets/Scripts/Generated/";

        [Button]
        public void GenerateEnums()
        {
            UnityEngine.Debug.Log($"Regenerating enums to {m_enumOutputFolder} ");

            List<string> declarations = new List<string>();
            List<string> enumVals = new List<string>();
            foreach (AiInputEntry input in InputTable.Inputs) { enumVals.Add(input.Name); }
            declarations.Add(EnumUtils.MakeEnumDeclaration("AiInputType", enumVals, "ushort", true));

            enumVals.Clear();
            foreach (AiConsiderationEntry consideration in ConsiderationTable.Considerations) { enumVals.Add(consideration.Name); }
            declarations.Add(EnumUtils.MakeEnumDeclaration("AiConsiderationType", enumVals, "ushort", true));

            enumVals.Clear();
            foreach (AiDecisionEntry decision in DecisionTable.Decisions) { enumVals.Add(decision.Name); } 
            declarations.Add(EnumUtils.MakeEnumDeclaration("AiDecisionType", enumVals, "ushort", true));

            enumVals.Clear();
            foreach (AiProfileEntry profile in ProfileTable.Profiles) { enumVals.Add(profile.Name); }
            declarations.Add(EnumUtils.MakeEnumDeclaration("AiProfileType", enumVals, "ushort", true));

            EnumUtils.WriteDeclarationsToFile("AiEnums.cs", declarations, true, m_enumOutputFolder);
        }

        [Button]
        public void ForceDomainReload()
        {
            UnityEditor.AssetDatabase.Refresh();
        }

        //#if UNITY_EDITOR
        //    private void OnValidate()
        //    {
        //        GenerateEnums();
        //    }
        //#endif
    }
}