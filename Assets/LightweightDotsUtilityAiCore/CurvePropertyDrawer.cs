// This contains property drawer stuff to allow adding animation curves to a scriptable object or standard editor script.
// credit: glamberous in the TMG discord

// ADD TO PROJECT
using UnityEngine;
using UnityEditor;

namespace Lwduai
{
    public class BoundedCurveAttribute : PropertyAttribute
    {
        public Rect bounds;
        public int guiHeight;

        public BoundedCurveAttribute(float xMin, float yMin, float xMax, float yMax, int height = 1)
        {
            this.bounds = new Rect(xMin, yMin, xMax, yMax);
            this.guiHeight = height;
        }

        public BoundedCurveAttribute(int height = 1)
        {
            this.bounds = new Rect(0, 0, 1, 1);
            this.guiHeight = height;
        }
    }

    // EDIT FOLDER
    [CustomPropertyDrawer(typeof(BoundedCurveAttribute))]
    public class BoundedCurveDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            BoundedCurveAttribute boundedCurve = (BoundedCurveAttribute)attribute;
            return EditorGUIUtility.singleLineHeight * boundedCurve.guiHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            BoundedCurveAttribute boundedCurve = (BoundedCurveAttribute)attribute;
            EditorGUI.BeginProperty(position, label, property);
            property.animationCurveValue = EditorGUI.CurveField(position, label, property.animationCurveValue, Color.green, boundedCurve.bounds);
            EditorGUI.EndProperty();
        }
    }
} // namespace