// This aspect holds the 3 dynamic buffers for an agent (input, consideration scores, and decision scores).
//
// It also holds a pointer to the blob asset for definitions, which is redundant, but it is useful
// for quick access without requiring jobs to fetch the singleton manually. (An init system fills it).
// It also points to each agent's AiWinnerData, which holds the current temporary winner for the agent during scoring.
// It contains useful functions for scoring, manipulating, and reading an agent's AI data.

using Unity.Burst;
using Unity.Entities;

namespace Lwduai
{
    // holds the result of a decision score operation
    public struct DecisionScoreResult
    {
        public float Score;
        public bool IsComplete;
    }

    [BurstCompile]
    public readonly partial struct AiAgentAspect : IAspect
    {
        public readonly DynamicBuffer<AiInputData> Inputs;
        public readonly DynamicBuffer<AiConsiderationScoreData> ConsiderationScores;
        public readonly DynamicBuffer<AiDecisionScoreData> DecisionScores;
        public readonly DynamicBuffer<AiProfileData> Profiles;
        public readonly RefRO<AiAgentTableData> DefTableRef; // points to the blob asset.
        public readonly RefRW<AiWinnerData> WinnerRef;

        // shortcut for winner ref access
        public AiWinnerData Winner
        {
            get => WinnerRef.ValueRO;
            set => WinnerRef.ValueRW = value;
        }

        // shortcut for definition table ref access (read-only)
        public ref AiTableDef DefTable
        {
            get => ref DefTableRef.ValueRO.Reference.Value;
        }

        // This is normally called by user defined expensive input functions do determine if they
        // should execute or not.
        // Returns true if the specified input is in need of calculation
        [BurstCompile]
        public bool NeedsCalculation(AiInputType inputId)
        {
            return Inputs[(ushort)inputId].NeedsCalculation();
        }

        // Resets all inputs
        // It normally is only called by the LightWeightDotsUtilityAi framework.
        [BurstCompile]
        public void ResetInputs()
        {
            for (ushort i = 0; i < Inputs.Length; i++) {
                ref AiInputData input = ref Inputs.ElementAt(i);
                input.RefCount = 0;
                input.IsFreshInput = false;
            }
        }

        // Resets all consideration evaluations
        // It normally is only called by the LightWeightDotsUtilityAi framework.
        [BurstCompile]
        public void ResetConsiderations()
        {
            // reset evaluated flag for all considerations to false (keeping the cached score in it)
            for (ushort c = 0; c < ConsiderationScores.Length; c++) {
                ConsiderationScores.ElementAt(c).IsEvaluated = false;
            }
        }

        // Resets all decisions
        // It normally is only called by the LightWeightDotsUtilityAi framework.
        [BurstCompile]
        public void ResetDecisions()
        {
            for (ushort d = 0; d < DecisionScores.Length; d++) {
                DecisionScores.ElementAt(d).IsComplete = false;
                DecisionScores.ElementAt(d).IsUsedByProfile = false;
            }
        }

        // This function is called by user defined expensive input jobs when they finish (if auto-score is disabled).
        // It will set the input value and then do a partial scoring.
        // It will decrement input ref counts as it runs, which may cause future jobs to skip running for some agents.
        [BurstCompile]
        public void ScoreInput(AiInputType inputId, float value)
        {
            // first set the new input value
            ref AiInputData inputRef = ref Inputs.ElementAt((ushort)inputId);
            inputRef.Value = value;
            inputRef.IsFreshInput = true;
            inputRef.RefCount = 0; // won't need calculation again in this interval

            // then run a partial scoring
            RunPartialScore();
        }

        // This function should be called at the beginning of frame 0 after the cheap inputs have run.
        // It will run the initial scoring and also set the IsUsedByProfile fields and increment ref counts.
        // It normally is only called by the LightWeightDotsUtilityAi framework.
        public void RunInitialScore()
        {
            // loop through each profile, and duplicate decisions will be skipped
            bool hasNewCompleteHighScore = false;
            for (ushort p = 0; p < Profiles.Length; p++) {
                ushort profileIndex = (ushort)Profiles[p].ProfileId;
                ref AiProfileDef profileDef = ref DefTable.Profiles[profileIndex];
                // for every decision in the profile...
                for (ushort d = 0; d < profileDef.DecisionIds.Length; d++) {
                    // Note that it's very possible that another profile calculated this decision.
                    ushort decisionIndex = (ushort)profileDef.DecisionIds[d];
                    // only run the scoring if no other profile tried to score it
                    if (!DecisionScores[decisionIndex].IsUsedByProfile) {
                        DecisionScoreResult scoreResult = ScoreDecision(decisionIndex);
                        if (scoreResult.IsComplete) {
                            // update winner (if this one won).
                            // mark this as complete, but don't decrement its refs
                            if (CompareDecision(decisionIndex, scoreResult.Score)) { hasNewCompleteHighScore = true; }
                            CompleteDecisionScore(decisionIndex, scoreResult.Score);
                        } else {
                            // set the temporary score
                            DecisionScores.ElementAt(decisionIndex).TemporaryScore = scoreResult.Score;
                            IncrementInputRefCounts(decisionIndex);
                        }
                        // Set is-used to true (this is the only place that does this)
                        DecisionScores.ElementAt(decisionIndex).IsUsedByProfile = true;
                    }
                }
            }

            if (hasNewCompleteHighScore) { SweepPreWinnerScores(); }
        }

        // This is a function that performs a partial scoring.
        // It loops through all incomplete scores, and attempts to complete them.
        // If a new high-score emerges, all other scores are compared and swept clean if their scores are too low.
        // It normally is only called by the LightWeightDotsUtilityAi framework.
        public void RunPartialScore()
        {
            // for any incomplete decisions, run them again.
            bool hasNewCompleteHighScore = false;
            for (ushort d = 0; d < DecisionScores.Length; d++) {
                if (DecisionScores[d].IsUsedByProfile) {
                    DecisionScoreResult score = ScoreDecision(d);
                    if (score.IsComplete) {
                        if (CompareDecision(d, score.Score)) { hasNewCompleteHighScore = true; }
                        CompleteDecisionScore(d, score.Score);
                        DecrementInputRefCounts(d);
                    }
                }
            }

            // If a new winner was found, then check all decisions before the winner (previous ones).
            // Maybe the new winner will make them complete. Note that the previous ones can only
            // be canceled for having low scores - they won't ever become winners, so it's not
            // necessary to use recursion or anything fancy.
            if (hasNewCompleteHighScore) { SweepPreWinnerScores(); }
        }

        // This is an nonpublic function that will check all incomplete scores before the winner index
        // and short them out if their score is too low.
        // Note that it does not do any evaluation so it is impossible to find a new high score in this function.
        [BurstCompile]
        void SweepPreWinnerScores()
        {
            for (ushort d = 0; d < Winner.Index; d++) {
                ref AiDecisionScoreData decisionScoreRef = ref DecisionScores.ElementAt(d);
                if (decisionScoreRef.IsUsedByProfile && !decisionScoreRef.IsComplete) {
                    if (decisionScoreRef.TemporaryScore < Winner.Score) {
                        CompleteDecisionScore(d, decisionScoreRef.TemporaryScore); // complete via score-to-low
                        DecrementInputRefCounts(d); // don't need this anymore
                    }
                }
            }
        }

        // Mark the specified score as complete and set its value
        [BurstCompile]
        void CompleteDecisionScore(ushort decisionIndex, float scoreVal)
        {
            ref AiDecisionScoreData score = ref DecisionScores.ElementAt(decisionIndex);
            score.IsComplete = true;
            score.Score = scoreVal;
        }


        // sets the winner if the specified score is higher
        // decisionIndex --> the index of the specified score
        // decisionScore --> the score to compare against the winner
        // returns true of the specified score is larger than the winner
        // It normally is only called by the LightWeightDotsUtilityAi framework.
        [BurstCompile]
        public bool CompareDecision(ushort decisionIndex, float decisionScore)
        {
            if (decisionScore > Winner.Score) {
                Winner = new AiWinnerData { Index = decisionIndex, Score = decisionScore };
                return true;
            }
            return false;
        }

        // This will score the specified decision and return its resulting score.
        // It can modify the consideration buffer if a new consideration needs to be evaluated (caches it).
        // It normally is only called by the LightWeightDotsUtilityAi framework.
        [BurstCompile]
        public DecisionScoreResult ScoreDecision(ushort decisionIndex)
        {
            ref AiDecisionDef decisionDef = ref DefTable.Decisions[decisionIndex];

            // Initialize the temporary score to the weight.
            DecisionScoreResult result = new DecisionScoreResult() { IsComplete = true, Score = decisionDef.Weight };

            // Ignore disabled decisions.
            if (!decisionDef.IsEnabled) { return result; }

            // For every consideration in this decision...
            for (ushort c = 0; c < decisionDef.ConsiderationIds.Length; c++) {
                // Easy-out if this score cannot possibly win. Since IsComplete was set to true above, it will be considered complete after the break.
                if ((result.Score <= 0f) || (result.Score < Winner.Score)) { break; }

                // Evaluate this consideration.
                // If it fails because a necessary input is not available it will return false.
                ushort considerationIndex = (ushort)decisionDef.ConsiderationIds[c];
                ref AiConsiderationDef considerationDef = ref DefTable.Considerations[considerationIndex];
                ref AiConsiderationScoreData considerationScoreRef = ref ConsiderationScores.ElementAt(considerationIndex);
                if (considerationDef.IsEvaluatable(Inputs)) {
                    EvaluateConsideration(ref considerationDef);
                    // Include this score in the final score.
                    // Skew for the consideration count (so that decisions with lots of considerations aren't at a disadvantage).
                    result.Score *= considerationScoreRef.Score + (((1 - considerationScoreRef.Score) * (1 - (1 / decisionDef.ConsiderationIds.Length))) * considerationScoreRef.Score);
                } else {
                    result.IsComplete = false;
                }
            }
            return result;
        }

        // This will evaluate the specified consideration.
        // You should first check if the consideration is evaluatable.
        // If successful, the specifed consideration in the buffer will be updated and marked as evaluated.
        // It normally is only called by the LightWeightDotsUtilityAi framework.
        [BurstCompile]
        public void EvaluateConsideration(ref AiConsiderationDef considerationDef)
        {
            // If execution reaches here, then all input ranges can be resolved.
            // At this point the curve can safely be evaluated.
            ushort inputIndex = (ushort)considerationDef.InputId;
            float max = considerationDef.GetMaxValue(Inputs);
            float min = considerationDef.GetMinValue(Inputs);
            float scaledInputValue = AiUtils.ScaleToClampedUnitInterval(Inputs[inputIndex].Value, min, max);

            // Evaluate and assign the new score
            ref AiConsiderationScoreData scoreRef = ref ConsiderationScores.ElementAt((ushort)considerationDef.ConsiderationId);
            scoreRef.Score = CurveUtils.Evaluate(scaledInputValue, considerationDef.Curve);
            scoreRef.IsEvaluated = true;
        }
        [BurstCompile]
        public void EvaluateConsideration(ushort considerationIndex) { EvaluateConsideration(ref DefTable.Considerations[considerationIndex]); }

        // Increment all ref counts for every consideration for the specified decision.
        // This is typically only ever done in the initial pass.
        [BurstCompile]
        void IncrementInputRefCounts(ref AiDecisionDef decisionDef)
        {
            for (ushort c = 0; c < decisionDef.ConsiderationIds.Length; c++) {
                ushort considerationIndex = (ushort)decisionDef.ConsiderationIds[c];
                if (!ConsiderationScores[considerationIndex].IsEvaluated) {
                    ref AiConsiderationDef considerationDef = ref DefTable.Considerations[considerationIndex];
                    // increment refs for this consideration
                    if (!Inputs[(ushort)considerationDef.InputId].IsFreshInput) { Inputs.ElementAt((ushort)considerationDef.InputId).RefCount++; }
                    if (considerationDef.MaxIsDynamic() && !Inputs[(ushort)considerationDef.Max].IsFreshInput) { Inputs.ElementAt((ushort)considerationDef.Max).RefCount++; }
                    if (considerationDef.MinIsDynamic() && !Inputs[(ushort)considerationDef.Min].IsFreshInput) { Inputs.ElementAt((ushort)considerationDef.Min).RefCount++; }
                }
            }
        }
        [BurstCompile]
        void IncrementInputRefCounts(ushort decisionIndex) { IncrementInputRefCounts(ref DefTable.Decisions[decisionIndex]); }


        // Decrements all input refs for every consideration of the specified decision.
        // Usually this is called after a decision is marked as complete.
        [BurstCompile]
        void DecrementInputRefCounts(ref AiDecisionDef decisionDef)
        {
            for (ushort c = 0; c < decisionDef.ConsiderationIds.Length; c++) {
                ushort considerationIndex = (ushort)decisionDef.ConsiderationIds[c];
                ref AiConsiderationDef considerationDef = ref DefTable.Considerations[considerationIndex];
                if (Inputs[(ushort)considerationDef.InputId].RefCount > 0) {
                    Inputs.ElementAt((ushort)considerationDef.InputId).RefCount--;
                }
                if (considerationDef.MinIsDynamic() && (Inputs[(ushort)considerationDef.Min].RefCount > 0)) {
                    Inputs.ElementAt((ushort)considerationDef.Min).RefCount--;
                }
                if (considerationDef.MaxIsDynamic() && (Inputs[(ushort)considerationDef.Max].RefCount > 0)) {
                    Inputs.ElementAt((ushort)considerationDef.Max).RefCount--;
                }
            }
        }
        [BurstCompile]
        void DecrementInputRefCounts(ushort decisionIndex) { DecrementInputRefCounts(ref DefTable.Decisions[decisionIndex]); }
    }
}