// this scriptable object defines a list of AI profiles.

using UnityEngine;
using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace Lwduai
{
    [System.Serializable]
    public class AiProfileEntry
    {
        //[OnValueChanged("@AiUtils.RegenerateEnums()")]
        [Tooltip("The name of the profile.")]
        public string Name = "New Profile";

        [Tooltip("A list of decisions that this profile will evaluate when choosing an action.")]
        [ValueDropdown("@AiOdinUtils.RunDecisionNameQuery()", ExcludeExistingValuesInList = true)]
        public List<string> DecisionNames;

        // returns the id of the specified decision, converted from a string
        public AiDecisionType MakeDecisionId(ushort idx) { return (AiDecisionType)System.Enum.Parse(typeof(AiDecisionType), DecisionNames[idx]); }

        // returns the id of the profile, converted from a string
        public AiProfileType MakeProfileId() { return (AiProfileType)System.Enum.Parse(typeof(AiProfileType), Name); }
    }


    [CreateAssetMenu(fileName = "New AI Profile Table", menuName = "Game/AI Profile Table")]
    public class AiProfileTableRecord : ScriptableObject
    {
        //[OnValueChanged("@AiUtils.RegenerateEnums()")]
        public List<AiProfileEntry> Profiles;

        //#if UNITY_EDITOR
        //    private void OnValidate()
        //    {
        //        if (!AiManager.ExistsInScene()) { return; }
        //        AiManager.Instance.GenerateEnums();
        //    }
        //#endif

        //public void ListChangeCallback()
        //{
        //    if (!AiManager.ExistsInScene()) { return; }
        //    AiManager.Instance.GenerateEnums();
        //}
    }
}