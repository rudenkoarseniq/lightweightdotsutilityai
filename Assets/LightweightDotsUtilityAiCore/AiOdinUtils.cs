// This class contains code to fill up dropdown lists for the utility AI scriptable objects.
// It requires Odin Inspector to be installed.
// This also requires an AiManager singleton, which is really only needed for this script.
//
// TODO: I would like to make a tabbed view which would remove the need for script,
// and hopefully remove the dependency on Odin Inspector.


using System.Collections;
using System.Linq;

namespace Lwduai
{
    public static class AiOdinUtils
    {
        static IEnumerable RunInputNameQuery()
        {
            return AiManager.Instance.InputTable.Inputs.Select(x => x.Name);
        }

        static IEnumerable RunConsiderationNameQuery()
        {
            return AiManager.Instance.ConsiderationTable.Considerations.Select(x => x.Name);
        }

        static IEnumerable RunDecisionNameQuery()
        {
            return AiManager.Instance.DecisionTable.Decisions.Select(x => x.Name);
        }

        static IEnumerable RunProfileNameQuery()
        {
            return AiManager.Instance.ProfileTable.Profiles.Select(x => x.Name);
        }
    }
}