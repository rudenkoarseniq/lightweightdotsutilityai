// This scriptable object defines a list of AI inputs.

using UnityEngine;
using System.Collections.Generic;
//using Sirenix.OdinInspector;

namespace Lwduai
{
    [System.Serializable]
    public class AiInputEntry
    {
        //[OnValueChanged("@AiUtils.RegenerateEnums()")]
        [Tooltip("The name of the input (default 'New Input')")]
        public string Name = "New Input";

        // returns the ID of input, converted from a string.
        public AiInputType MakeId() { return (AiInputType)System.Enum.Parse(typeof(AiInputType), Name); }
    }

    [CreateAssetMenu(fileName = "New AI Input Table", menuName = "Game/AI Input Table")]
    public class AiInputTableRecord : ScriptableObject
    {
        //[OnValueChanged("@AiUtils.RegenerateEnums()")]
        public List<AiInputEntry> Inputs;

        //#if UNITY_EDITOR
        //    private void OnValidate()
        //    {
        //        if (!AiManager.ExistsInScene()) { return; }
        //        AiManager.Instance.GenerateEnums();
        //    }
        //#endif
    }
}