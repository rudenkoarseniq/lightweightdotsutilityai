// Typically there is only one of these in the scene.
// You can configure it in the editor.
// It creates a singleton that contains profiles, which are made of decisions,
// which are made of considerations, which are made of inputs.
//
// Press the GenerateEnums button to generate enums that can be used to reference various inputs/considerations/decisions/profiles.
//
// 0.25 * 30 -> 7.5 frames
// 0.25 * 60 --> 15 frames

using UnityEngine;
using System.Collections.Generic;
using Unity.Entities;

namespace Lwduai
{
    [System.Serializable]
    public struct AiTableEntry
    {
        public List<AiInputEntry> Inputs;
        public List<AiConsiderationEntry> Considerations; // all possible considerations
        public List<AiDecisionEntry> Decisions; // all possible decision
        public List<AiProfileEntry> Profiles; // all possible profiles
    }

    public struct AiFrameStateData : IComponentData
    {
        // Frame -1 is the normal state (scoring not running)
        // Frames 0..N-1 are the expensive input frames
        // Frame N is the final reaction system frame.
        public sbyte FrameNumber;
    }

    public struct AiScoreConfigData : IComponentData
    {
        public sbyte LastFrame; // this is "N" - the last frame (the frame where the action is fresh and presented to the user)
        public float ScoringInterval; // the interval at which scoring happens
        public bool EnableAutoEndFrameScore; // true to enable an end-frame scoring system
    }

    public class AiSingletonAuthoring : MonoBehaviour
    {
        [Tooltip("These are extra frames that you can use for filling expensive inputs that are action depedent. See docs. (default 0).")]
        [Sirenix.OdinInspector.PropertyRange(0, 8)]
        public sbyte ExtraFrameCount = 0;

        [Tooltip("Set this to your game's target FPS - it is only used to warn if you if your config is invalid. (default 30)")]
        [Sirenix.OdinInspector.PropertyRange(3, 240)]
        public ushort TargetFPS = 30;

        [Tooltip("The interval at which scoring happens, measured in seconds. (default 0.25)")]
        [Sirenix.OdinInspector.PropertyRange(0.01666666666, 10)]
        public float ScoringInterval = 0.25f;

        [Tooltip("Auto-runs a scoring at the end of each frame. See docs. (default true)")]
        public bool EnableAutoEndFrameScore = true;

        [Tooltip("A list of all possible inputs")]
        public AiInputTableRecord InputTable;

        [Tooltip("A list of all possible considerations")]
        public AiConsiderationTableRecord ConsiderationTable;

        [Tooltip("A list of all possible decisions")]
        public AiDecisionTableRecord DecisionTable;

        [Tooltip("A list of all possible profiles")]
        public AiProfileTableRecord ProfileTable;

        // this is for showing an info box when the configuration is incorrect.
        #if UNITY_EDITOR
            [Sirenix.OdinInspector.OnInspectorGUI, Sirenix.OdinInspector.PropertyOrder(int.MaxValue)]
            private void DrawInfoBox()
            {
                if (ExtraFrameCount < 0) {
                    Sirenix.Utilities.Editor.SirenixEditorGUI.WarningMessageBox("ExtraFrameCount must be >= 0!!");
                } else if ((ExtraFrameCount + 2) >= (ScoringInterval * TargetFPS)) {
                    Sirenix.Utilities.Editor.SirenixEditorGUI.WarningMessageBox($"Extra frame count is {ExtraFrameCount} but there are only {Unity.Mathematics.math.floor(ScoringInterval * TargetFPS) - 2} extra frames available at {TargetFPS} FPS! Increase scoring interval or target FPS, or decrease scoring frame count.");
                }
            }
        #endif

        private void OnValidate()
        {
            if (!AiManager.ExistsInScene()) { return; }

            // copy the SO references to AiManager so it can use them as well.
            AiManager.Instance.InputTable = InputTable;
            AiManager.Instance.ConsiderationTable = ConsiderationTable;
            AiManager.Instance.DecisionTable = DecisionTable;
            AiManager.Instance.ProfileTable = ProfileTable;
        }
    }

    public class AiSingletonBaker : Baker<AiSingletonAuthoring>
    {
        public override void Bake(AiSingletonAuthoring authoring)
        {
            DependsOn(authoring.InputTable);
            DependsOn(authoring.ConsiderationTable);
            DependsOn(authoring.DecisionTable);
            DependsOn(authoring.ProfileTable);

            Entity entity = GetEntity(authoring, TransformUsageFlags.None);

            AiTableEntry data = new AiTableEntry { Inputs = authoring.InputTable.Inputs, Considerations = authoring.ConsiderationTable.Considerations, Decisions = authoring.DecisionTable.Decisions, Profiles = authoring.ProfileTable.Profiles };
            AddComponent(entity, new AiTableData { Reference = AiUtils.CreateBlobAsset(data) });
            AddComponent(entity, new AiFrameStateData { FrameNumber = -1 });
            AddComponent(entity, new AiScoreConfigData { LastFrame = (sbyte)(authoring.ExtraFrameCount + 1), ScoringInterval = authoring.ScoringInterval, EnableAutoEndFrameScore = authoring.EnableAutoEndFrameScore });
        }
    }
}