// this scriptable object defines a list of AI decisions.

using UnityEngine;
using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace Lwduai
{
    [System.Serializable]
    public class AiDecisionEntry
    {
        //[OnValueChanged("@AiUtils.RegenerateEnums()")]
        [Tooltip("Name of the decision.")]
        public string Name = "New Decision";

        [Tooltip("Weight is an extra fudge factor to give advantage to a decision. (default 1).")]
        public float Weight = 1f;

        [Tooltip("Unchecking this can disable the decision (useful for debugging) (default true).")]
        public bool IsEnabled = true;

        [Tooltip("A list of all the considerations that factor into the decision.")]
        [ValueDropdown("@AiOdinUtils.RunConsiderationNameQuery()", ExcludeExistingValuesInList = true)]
        public List<string> ConsiderationNames;

        // returns the id of the specified consideration index, converted from a string
        public AiConsiderationType MakeConsiderationId(ushort idx) { return (AiConsiderationType)System.Enum.Parse(typeof(AiConsiderationType), ConsiderationNames[idx]); }

        // returns the id of the decision, converted from a string
        public AiDecisionType MakeId() { return (AiDecisionType)System.Enum.Parse(typeof(AiDecisionType), Name); }
    }

    [CreateAssetMenu(fileName = "New AI Decision Table", menuName = "Game/AI Decision Table")]
    public class AiDecisionTableRecord : ScriptableObject
    {
        //[OnValueChanged("@AiUtils.RegenerateEnums()")]
        public List<AiDecisionEntry> Decisions;

        //#if UNITY_EDITOR
        //    private void OnValidate()
        //    {
        //        if (!AiManager.ExistsInScene()) { return; }
        //        AiManager.Instance.GenerateEnums();
        //    }
        //#endif
    }
}